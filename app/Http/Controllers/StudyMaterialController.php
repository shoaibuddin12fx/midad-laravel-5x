<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Request;
use Storage;
use Zip;
use ZipStream\ZipStream;
use ZipStream\Option\Archive as ArchiveOptions;
use Illuminate\Contracts\Filesystem\Filesystem;
// to stream the file to the browser
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;


class StudyMaterialController extends Controller {

	var $data = array();
	var $panelInit ;
	var $layout = 'dashboard';

	public function __construct(){
		if(app('request')->header('Authorization') != "" || \Input::has('token')){
			$this->middleware('jwt.auth');
		}else{
			$this->middleware('authApplication');
		}

		$this->panelInit = new \DashboardInit();
		$this->data['panelInit'] = $this->panelInit;
		$this->data['users'] = $this->panelInit->getAuthUser();
		if(!isset($this->data['users']->id)){
			return \Redirect::to('/');
		}

	}

	public function listAll( $page = 1 )
	{

		if(!$this->panelInit->can( array("studyMaterial.list","studyMaterial.addMaterial","studyMaterial.editMaterial","studyMaterial.delMaterial","studyMaterial.Download") )){
			exit;
		}

		$toReturn = array();

		if($this->data['users']->role == "teacher"){
			$toReturn['classes'] = \classes::where('classAcademicYear',$this->panelInit->selectAcYear)->where('classTeacher','LIKE','%"'.$this->data['users']->id.'"%')->get()->toArray();
		}else{
			$toReturn['classes'] = \classes::where('classAcademicYear',$this->panelInit->selectAcYear)->get()->toArray();
		}

		$classesArray = array();

		foreach($toReturn['classes'] as $class){
			$classesArray[$class['id']] = $class['className'];
		}

		$subjects = \subject::get()->toArray();

		if($this->data['users']->role == "student"){

		    // find class of student
            $studentClass = $this->data['users']->studentClass;

            // find subjects of the class
            $stdclass = \classes::where('id', $studentClass )->first();
            if(!$stdclass){
                return array();
            }

            if(!isset($stdclass->classSubjects) || $stdclass->classSubjects == ""){
                return array();
            }

            $classSubjects = json_decode($stdclass->classSubjects, true);
            $subjects = \subject::whereIn('id', $classSubjects)->get()->toArray();
        }

        if($this->data['users']->role == "teacher"){

            $tmp = [];
            foreach ($subjects as $subject){
                $teacherId = strval($this->data['users']->id);

                    if(isset($subject['teacherId']) && $subject['teacherId'] != "" ){
                        $teachers = json_decode($subject['teacherId']);
                        if( in_array($teacherId, $teachers) )   {
                            $tmp[] = $subject;
                        }
                        // find teacher in the list
                    }
            }

            $subjects = $tmp;


        }


		$subjectArray = array();
		foreach($subjects as $subject){
			$subjectArray[$subject['id']] = $subject['subjectTitle'];
		}

		$toReturn['materials'] = array();
		$studyMaterial = new \study_material();

		if($this->data['users']->role == "student"){
			$studyMaterial = $studyMaterial->where('class_id','LIKE','%"'.$this->data['users']->studentClass.'"%');
			if($this->panelInit->settingsArray['enableSections'] == true){
				$studyMaterial = $studyMaterial->where('sectionId','LIKE','%"'.$this->data['users']->studentSection.'"%');
			}
		}elseif($this->data['users']->role == "parent"){

			if(!is_array($this->data['users']->parentOf)){
				$parentOf = json_decode($this->data['users']->parentOf,true);
			}else{
				$parentOf = $this->data['users']->parentOf;
			}

			if(!is_array($parentOf) || count($parentOf) == 0){
				return array();
			}

			$std_id = array();
			foreach ($parentOf as $key => $value) {
				$std_id[] = $value['id'];
			}

			$students = \User::whereIn('id',$std_id)->select('studentClass','studentSection');

			if( $students->count() > 0 ){

				$classes = array();
				$sections = array();
				$students = $students->get();
				foreach ($students as $key => $value) {
					$classes[] = $value->studentClass;
					$sections[] = $value->studentSection;
				}

				if( count($classes) == 0 ){
					return array();
				}else{
					$studyMaterial = $studyMaterial->where(function($query) use ($classes){
												foreach ($classes as $key => $value) {
													$query->orWhere('class_id','LIKE','%"'.$value.'"%');
												}
											});
					
					if($this->panelInit->settingsArray['enableSections'] == true AND count($sections) > 0){
						$studyMaterial = $studyMaterial->where(function($query) use ($sections){
							foreach ($sections as $key => $value) {
								$query->orWhere('sectionId','LIKE','%"'.$value.'"%');
							}
						});
					}
				}

			}else{
				return array();
			}

		}elseif($this->data['users']->role == "teacher"){
			$studyMaterial = $studyMaterial->where('teacher_id',$this->data['users']->id);
		}

		if( \Input::has('searchInput') ){
			$keyword = \Input::get('searchInput');
			$studyMaterial = $studyMaterial->where(function($query) use ($keyword){
											$query->where('material_title','like','%'.$keyword.'%')->orWhere('material_description','like','%'.$keyword.'%');
										});
		}

		// Add Subjects in return of response
		$selectedSubject = \Input::get('selectedSubject');
		if( isset($selectedSubject) && $selectedSubject != ""  ){
			$studyMaterial = $studyMaterial->where('subject_id', $selectedSubject);
		}


		$toReturn['totalItems'] = $studyMaterial->count();
		$toReturn['subjects'] = $subjects;
		$toReturn['selectedSubject'] = $selectedSubject;

		$studyMaterial = $studyMaterial->orderby('id','desc')->take('20')->skip( 20 * ($page - 1) )->get();

		foreach ($studyMaterial as $key => $material) {
			$classId = json_decode($material->class_id);
			if($this->data['users']->role == "student" AND !in_array($this->data['users']->studentClass, $classId)){
				continue;
			}
			$toReturn['materials'][$key]['id'] = $material->id;
			$toReturn['materials'][$key]['subjectId'] = $material->subject_id;
			if(isset($subjectArray[$material->subject_id])){
				$toReturn['materials'][$key]['subject'] = $subjectArray[$material->subject_id];
			}else{
				$toReturn['materials'][$key]['subject'] = "";
			}
			$toReturn['materials'][$key]['material_title'] = $material->material_title;
			$toReturn['materials'][$key]['material_description'] = $material->material_description;
			
			$toReturn['materials'][$key]['classes'] = "";

            if(is_array($classId)){
            	foreach($classId as $value){
    				if(isset($classesArray[$value])) {
    					$toReturn['materials'][$key]['classes'] .= $classesArray[$value].", ";
    				}
    			}
			}

			
			$locations = [];
			if(isset($material->material_file) && $material->material_file != "" ){
				$filesarray = json_decode($material->material_file, true);
				foreach($filesarray as $file){
					$locations[] = $this->returnFileLocation($file);
				}
			}

			$toReturn['materials'][$key]['material_file'] = $locations ;



		}

		$toReturn['userRole'] = $this->data['users']->role;
		return $toReturn;
		exit;
	}

	public function delete($id){

		if(!$this->panelInit->can( "studyMaterial.delMaterial" )){
			exit;
		}

		if ( $postDelete = \study_material::where('id', $id)->first() )
        {
			// @unlink('uploads/studyMaterial/'.$postDelete->material_file);


			if(isset($postDelete->material_file) && $postDelete->material_file != ""){
				$files = json_decode($postDelete->material_file, true);						
				foreach($files as $file ){
					$this->deleteFileToS3($file);
				}
			}
			
            $postDelete->delete();
            return $this->panelInit->apiOutput(true,$this->panelInit->language['delMaterial'],$this->panelInit->language['materialDel']);
        }else{
            return $this->panelInit->apiOutput(false,$this->panelInit->language['delMaterial'],$this->panelInit->language['materialNotExist']);
        }
	}

	public function create(){

		if(!$this->panelInit->can( "studyMaterial.addMaterial" )){
			exit;
		}

		$studyMaterial = new \study_material();
		$studyMaterial->class_id = json_encode(\Input::get('class_id'));
		if($this->panelInit->settingsArray['enableSections'] == true){
			$studyMaterial->sectionId = json_encode(\Input::get('sectionId'));
		}
		$studyMaterial->subject_id = \Input::get('subject_id');
		$studyMaterial->material_title = \Input::get('material_title');
		$studyMaterial->material_description = \Input::get('material_description');
		$studyMaterial->teacher_id = $this->data['users']->id;
		
		
		if (\Input::hasFile('material_file')) {

			$files = \Input::file('material_file');
			$file_locations = [];

			if(count($files) > 3){
				return $this->panelInit->apiOutput(false,$this->panelInit->language['addMaterial'],"You can only upload 3 files max");
			}

			$size = 0;
			foreach($files as $fileInstance){
				$size = $size + $fileInstance->getSize();
			}

			$size = $size / 1000000; 

			if($size > 500){
				return $this->panelInit->apiOutput(false,$this->panelInit->language['addMaterial'],"$size MB , You can only upload 500 MB max");
			}

			// File types are restricted to (ppt , pptx, doc , docx , all video & voice)
			foreach($files as $fileInstance){
				
				if(!$this->validate_studyMaterial_upload($fileInstance)){
					return $this->panelInit->apiOutput(false,$this->panelInit->language['addMaterial'],"Sorry, This File Type Is Not Permitted For Security Reasons ");
				}

			}
			
			foreach( $files as $fileInstance){
                $file_locations[] = $this->uploadFileToS3($fileInstance);				
			}

			$studyMaterial->material_file = json_encode($file_locations);
			

			
		}

		$studyMaterial->save();

		//Send Push Notifications
		$tokens_list = array();
		$user_list = \User::where('role','student')->whereIn('studentClass',\Input::get('class_id'));
		if($this->panelInit->settingsArray['enableSections'] == true){
			$user_list = $user_list->whereIn('studentSection',\Input::get('sectionId'));
		}
		$user_list = $user_list->select('firebase_token')->get();
		
		foreach ($user_list as $value) {
			if($value['firebase_token'] != ""){
				$tokens_list[] = $value['firebase_token'];				
			}
		}

		if(count($tokens_list) > 0){
			$this->panelInit->send_push_notification($tokens_list,$this->panelInit->language['materialNotif']." : ".\Input::get('material_title'),$this->panelInit->language['studyMaterial'],"material");			
		}

		return $this->panelInit->apiOutput(true,$this->panelInit->language['addMaterial'],$this->panelInit->language['materialAdded'],$studyMaterial->toArray() );
	}

	function fetch($id){

		if(!$this->panelInit->can( "studyMaterial.editMaterial" )){
			exit;
		}

		$studyMaterial = \study_material::where('id',$id)->first();

		$locations = [];
		if(isset($studyMaterial->material_file) && $studyMaterial->material_file != "" ){
			$filesarray = json_decode($studyMaterial->material_file, true);
			foreach($filesarray as $file){
				$locations[] = $this->returnFileLocation($file);
			}
		}

		$studyMaterial['material_uploaded_files'] = $locations;

		$studyMaterial = $studyMaterial->toArray();




		$DashboardController = new DashboardController();
		$studyMaterial['sections'] = $DashboardController->sectionsList(json_decode($studyMaterial['class_id'],true));
		$studyMaterial['subject'] = $DashboardController->subjectList(json_decode($studyMaterial['class_id'],true));
		$studyMaterial['class_id'] = json_decode($studyMaterial['class_id'],true);
		return $studyMaterial;
	}




	public function download($id){

        $result = [
            'flag' => false
        ];

		if(!$this->panelInit->can( "studyMaterial.Download" )){
            $result['message'] = "Record does not exist, Please contact site administrator to reupload it again.";
            return $result;
//            echo "<br/><br/><br/><br/><br/><center>Record does not exist, Please contact site administrator to reupload it again.</center>";

		}

		$material = \study_material::where('id',$id)->first();

		if(!$material){
            $result['message'] = "Record does not exist, Please contact site administrator to reupload it again.";
            return $result;
//			echo "<br/><br/><br/><br/><br/><center>Record does not exist, Please contact site administrator to reupload it again.</center>";
		}

		//




        $request = app('request');

		$data = $material->material_file;
		$folder = "uploads/studyMaterial/";
		//$fileName = preg_replace('/[^a-zA-Z0-9-_\.]/','',$material->material_title). ".zip";
        $fileName = $material->material_title;




		if(isset($data) && $data != ""){
			$names = json_decode($material->material_file, true);

			// return response()->stream(function () use ($names, $folder, $fileName, $request) {

			//  $opt = new ArchiveOptions();
			// 	header("Content-Disposition: attachment; filename=" . $fileName . ".zip");
            //     header("Content-Type: application/x-zip-compressed");

			// 	$opt->setContentType('application/force-download');
			// 	$zip = new ZipStream( $fileName,$opt);

			// 	foreach ($names as $name) {
			// 		$path = $folder . $name;
			// 		$file = \Storage::disk("s3")->read($path);
			// 		$zip->addFile($name, $file);
			// 	}

			// 	$zip->finish();



            //     echo "";

			// });


            // Add file to zip



            $zipname = 'studyMaterial/' . 'material_' . $id . '.zip';
            $public_dir = public_path($zipname);

            if( file_exists($public_dir) ){
                $result['message'] = "File Download in progress ... please wait";
                $result['flag'] = true;
                $result['link'] = asset(  '/public/' . $zipname);
                return $result;
            }




//            $zipfile = $path . '/'. urlencode($fileName) . '.zip';

//            $zipfile = asset('storage' . '/'. urlencode($fileName) . '.zip');

            $zipfile = $public_dir;

//            return $public_dir;

            $zip = new \ZipArchive;
            $zip->open($public_dir, \ZipArchive::CREATE);

            $tempnamearray = [];
            foreach ($names as $name) {

                $path = $folder . $name;
//                $source = \Storage::disk("s3")->get($path);
//                $obj = $this->returnFileLocation($name);

//                $zip->put($name, $source);
//                $zip->addFile($obj['link'], urlencode($name));

//                $path = $folder . $name;
                $source = \Storage::disk("s3")->readStream($path);
//
                $tmpfname = public_path('studyMaterial/' . $name);
                $destination = fopen($tmpfname, "w");
                while (!feof($source)) {
                    fwrite($destination, fread($source,8192));
                }


                fclose($source);
                fclose($destination);     

                $zip->addFile($tmpfname, urlencode($name));

                $tempnamearray[] = $tmpfname;







            }

            $zip->close();

            foreach($tempnamearray as $tmane) {
                unlink($tmane);
            }

            $result['message'] = "File Download in progress ... please wait";
            $result['flag'] = true;
            $result['link'] = asset('/public/'. $zipname);
            return $result;

            //\Storage::disk('s3')->put($folder . '/' . urlencode($fileName) . '.zip', $public_dir);
//            unlink($zipfile);

//            return $this->returnFileLocation(urlencode($fileName) . '.zip');






		}else{
            $result['message'] = "File not exist, Please contact site administrator to reupload it again.";
            return $result;
//			echo "<br/><br/><br/><br/><br/><center>File not exist, Please contact site administrator to reupload it again.</center>";
		}
		



		

		// $toReturn = \study_material::where('id',$id)->first();

		// echo json_decode($toReturn->material_file, true);

		// echo "<br/><br/><br/><br/><br/><center>File not exist, Please contact site administrator to reupload it again.</center>";
		// exit;

		// if(file_exists('uploads/studyMaterial/'.$toReturn->material_file)){
		// 	$fileName = preg_replace('/[^a-zA-Z0-9-_\.]/','',$toReturn->material_title). "." .pathinfo($toReturn->material_file, PATHINFO_EXTENSION);
		// 	header("Content-Type: application/force-download");
		// 	header("Content-Disposition: attachment; filename=" . $fileName);
		// 	echo file_get_contents('uploads/studyMaterial/'.$toReturn->material_file);
		// }else{
		// 	echo "<br/><br/><br/><br/><br/><center>File not exist, Please contact site administrator to reupload it again.</center>";
		// }
		// exit;
	}

	function edit($id){

		if(!$this->panelInit->can( "studyMaterial.editMaterial" )){
			exit;
		}
		
		$studyMaterial = \study_material::find($id);
		$studyMaterial->class_id = json_encode(\Input::get('class_id'));
		if($this->panelInit->settingsArray['enableSections'] == true){
			$studyMaterial->sectionId = json_encode(\Input::get('sectionId'));
		}
		$studyMaterial->subject_id = \Input::get('subject_id');
		$studyMaterial->material_title = \Input::get('material_title');
		$studyMaterial->material_description = \Input::get('material_description');
		
		$material_uploaded_files = \Input::get('material_uploaded_files');

		
		$saved_files = [];
		$previous_filecount = 0;
		$saved_filecount = 0;
		$files_to_keep = [];

		if(isset($material_uploaded_files) && $material_uploaded_files != ""){			
			$previous_filecount = count($material_uploaded_files);
			foreach($material_uploaded_files as $pre_file){
				$files_to_keep[] = json_decode($pre_file, true)['name'];
			}
		}

		if(isset($studyMaterial->material_file) && $studyMaterial->material_file != ""){
			$saved_files = json_decode($studyMaterial->material_file, true);
			$saved_filecount = count($saved_files);
		}

		// // find which files are removed from array
		$files_to_remove = array_diff($saved_files,$files_to_keep);
		$remove_count = count($files_to_remove);

		if (\Input::hasFile('material_file')) {
			$files = \Input::file('material_file');


			if($saved_filecount - $remove_count + count($files)  > 3){
				return $this->panelInit->apiOutput(false,$this->panelInit->language['editMaterial'],"You can only upload 3 files max");
			}

			$size = 0;
			foreach($files as $fileInstance){
				$size = $size + $fileInstance->getSize();
			}

			$size = $size / 1000000; 

			if($size > 500){
				return $this->panelInit->apiOutput(false,$this->panelInit->language['editMaterial'],"$size MB , You can only upload 500 MB max");
			}

			

			// File types are restricted to (ppt , pptx, doc , docx , all video & voice)
			foreach($files as $fileInstance){
				
				if(!$this->validate_studyMaterial_upload($fileInstance)){
					return $this->panelInit->apiOutput(false,$this->panelInit->language['editMaterial'],"Sorry, This File Type Is Not Permitted For Security Reasons ");
				}
				
			}

			

			// File types are restricted to (ppt , pptx, doc , docx , all video & voice)
			foreach( $files as $fileInstance){
				$files_to_keep[] = $this->uploadFileToS3($fileInstance);
			}

			
		}
		
		// unlink all previous files
		foreach( $files_to_remove as $fileInstance){
			$this->deleteFileToS3($fileInstance);			
		}

		$studyMaterial->material_file = json_encode($files_to_keep, true);
		

		$studyMaterial->save();
		$studyMaterial = $studyMaterial->toArray();

		
		$locations = [];
		if(isset($studyMaterial['material_file']) && $studyMaterial['material_file'] != "" ){
			$filesarray = json_decode($studyMaterial['material_file'], true);
			foreach($filesarray as $file){
				$locations[] = $this->returnFileLocation($file);
			}
		}

		$studyMaterial['material_file'] = $locations;

		return $this->panelInit->apiOutput(true,$this->panelInit->language['editMaterial'],$this->panelInit->language['materialEdited'], $studyMaterial );
	}

	public function validate_studyMaterial_upload($file){
		$allowed_mime_type = array("application/powerpoint", "application/msword", 
									"audio/midi", "audio/mpeg", "audio/webm", "audio/ogg", "audio/wav","audio/mpeg3", "audio/x-mpeg-3", "audio/m4a","audio/x-wav","audio/3gpp","audio/3gpp2","audio/x-realaudio",
									"video/webm", "video/ogg","video/mpeg", "video/x-mpeg","video/mp4","video/x-m4v","video/quicktime","video/x-ms-asf","video/x-ms-wmv","application/x-troff-msvideo", "video/avi", "video/msvideo", "video/x-msvideo","video/3gpp","video/3gpp2","video/x-flv","video/divx","video/x-matroska",
								);
		
		$allowed_extensions = array("doc","docx","ppt", "txt", "MP4", "mp4", "mp3", "MP3", "jpg", "jpeg", "png");

		$uploaded_mime_type = $file->getMimeType();
		$uploaded_extension = $file->getClientOriginalExtension();
		
		if( in_array( $uploaded_mime_type , $allowed_mime_type) 
			|| in_array( $uploaded_extension , $allowed_extensions ) ){
			return true;
		}

		return false;
	}

	function returnFileLocation($name){
		$amazon = 'https://midad.s3.eu-central-1.amazonaws.com/uploads/studyMaterial/';

		return [
			'name' => $name,
			'link' => $amazon . $name,
			'checked' => true,
			'isvideo' => $this->isVideo($name),
			'ext' => $ext = pathinfo($name, PATHINFO_EXTENSION)
		];

	}

	function isVideo($filename){
		$videoextensions = [ "WEBM","MPG", "MP2", "MPEG", "MPE", "MPV", "OGG", "MP4", "M4P", "M4V", "AVI", "WMV", "MOV", "QT", "FLV", "SWF", "AVCHD" ];
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		return in_array(strtoupper($ext), $videoextensions);
	}

	function uploadFileToS3($fileInstance){

	    // upload this file to s3
        $name = "material_".uniqid().".".$fileInstance->getClientOriginalExtension();
		$pathAndFileName = "uploads/studyMaterial/$name";
		$path = Storage::disk('s3')->put($pathAndFileName, fopen($fileInstance, 'r+'), 'public');
        if ( $path === true ) {
            $path = $name;
        }
        return $name;
	}
	
	function deleteFileToS3($name){

	    // upload this file to s3
		$pathAndFileName = "uploads/studyMaterial/$name";
		if(Storage::disk('s3')->exists($pathAndFileName)) {
			Storage::disk('s3')->delete($pathAndFileName);
		}
		
        return $name;
    }




}
