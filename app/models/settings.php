<?php
namespace app\models;
use Illuminate\Database\Eloquent\Model as Eloquent;

class settings extends Eloquent {
	public $timestamps = false;
	protected $table = 'settings';
}